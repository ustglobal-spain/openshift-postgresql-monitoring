.PHONY: help
.DEFAULT_GOAL := help

export BUILD_PROJECT?=roadlog-images
export PROJECT?=eld-field-test
export DATA_SOURCE_URI?=postgresql/?sslmode=disable


set-build: ## set the application
	oc new-app -f openshift/application-template-dockerbuild.yaml \
		-n ${BUILD_PROJECT}

set-app: ## set the application
	oc new-app -f openshift/application-template-exporter.yaml \
		-p DATA_SOURCE_URI=${DATA_SOURCE_URI} \
		-p APP_ENV=${PROJECT} \
		-p IMAGE_NAMESPACE=${BUILD_PROJECT} \
		-n ${PROJECT}


build-app: ## build the project
	oc start-build postgres-exporter --wait=true \
		-n ${BUILD_PROJECT}

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
